package main

import "container/heap"

type RuneHeap []rune

func (h RuneHeap) Len() int            { return len(h) }
func (h RuneHeap) Less(i, j rune) bool { return h[i] < h[j] }
func (h RuneHeap) Swap(i, j rune)      { h[i], h[j] = h[j], h[i] }

func (h *RuneHeap) Push(x interface{}) {
	*h = append(*h, x.(rune))
}

func (h *RuneHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
