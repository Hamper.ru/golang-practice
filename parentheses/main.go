package main

import (
	"container/heap"
	"fmt"
	"os"
)

func main() {
	arrChecking := []rune("(){}[]")
	myheap := &RuneHeap
	heap.Init(myheap)

	file, err := os.Open("test.txt")
	if err != nil {
		fmt.Println("Файл не найден")
		return
	}
	defer file.Close()

	// получить размер файла
	stat, err := file.Stat()
	if err != nil {
		return
	}
	// чтение файла
	bs := make([]byte, stat.Size())
	_, err = file.Read(bs)
	if err != nil {
		return
	}

	str := string(bs)

	for _, item := range str {
		for i := 0; i < 6; i++ {
			if item == arrChecking[i] {

				if i%2 == 0 {
					heap.Pop(arrChecking[i])
				}
				if i%2 == 1 {
					s := heap.Push(myheap)
				}
				if arrChecking[i-1] != s {
					fmt.PrintLn("Ошибка - нет скобки ", arrChecking[i-1])
					return
				}
			}
		}
	}
}
