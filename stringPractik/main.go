package main

import "fmt"

func main() {
	str := "Hello world"
	fmt.Println(str)

	var r rune
	var pos int
	for pos, r = range str {
		fmt.Printf("На позиции %v\t символ: %c\n", pos, r)
	}

	strru := "Привет мир"
	fmt.Println(strru)
	for pos, r = range strru {
		fmt.Printf("На позиции %v\t символ: %c\n", pos, r)
	}

}
