package main

import "fmt"

type lifo struct {
	Data  []string
	index int
}

func (stack *lifo) New(size int) error {
	if size < 1 {
		return fmt.Errorf("queue size = %v, entered stack size less than 1", size)
	}
	stack.Data = make([]string, size)
	stack.index = -1
	return nil
}

func (stack *lifo) Push(value string) error {
	fmt.Printf("Push(%v)\n", value)
	if stack.IsFull() {
		return fmt.Errorf("queue full stack")
	}
	stack.index++
	stack.Data[stack.index] = value
	return nil
}

func (stack *lifo) Pop() (string, error) {
	var value string
	var err error
	if stack.IsEmpty() {
		value = ""
		err = fmt.Errorf("queue empty")
	} else {
		value = stack.Data[stack.index]
		err = nil
		stack.index--
	}
	fmt.Println("Pop() value- ", value, " error-", err)
	return value, err
}

func (stack *lifo) Peek() (string, error) {
	var value string
	var err error
	if stack.IsEmpty() {
		value = ""
		err = fmt.Errorf("queue empty")
	} else {
		value = stack.Data[stack.index]
		err = nil
	}
	fmt.Println("Peek() ", value, " ", err)
	return value, err
}

func (stack *lifo) IsEmpty() bool {
	return stack.index < 0
}

func (stack *lifo) IsFull() bool {
	index := stack.index + 1
	size := len(stack.Data)
	fmt.Println("Size-", index, " Len-", size)
	return index >= size
}

func main() {
	var str string
	fmt.Println("Main START")
	var queue lifo
	queue.New(10)
	fmt.Println(queue.Peek())
	fmt.Println("IsEmpty ", queue.IsEmpty())
	queue.Push("aa  0 ")
	queue.Push("Привет 1 ")
	queue.Push("aa  2 ")
	queue.Push("aa  3 ")
	queue.Push("При 4 ")
	queue.Push("При 5 ")

	queue.Push("При 6 ")
	queue.Push("При 7 ")
	queue.Push("При 8 ")
	fmt.Println(queue.Push("При 9 "))
	fmt.Println(queue.Push("При 10 "))
	fmt.Println(queue.Push("При 11 "))
	str, _ = queue.Peek()
	fmt.Println(queue.Data)
	fmt.Println(str)
	fmt.Println("IsFull() ", queue.IsFull())
	fmt.Println("IsEmpty ", queue.IsEmpty())
	fmt.Println(queue.Pop())
	fmt.Println(queue.Pop())
	fmt.Println("IsEmpty ", queue.IsEmpty())
	//fmt.Printf("Peek() value-%v error-%v", queue.Peek())
	fmt.Println(queue.Pop())
	fmt.Println("IsEmpty ", queue.IsEmpty())
	fmt.Println("Main END")

}
