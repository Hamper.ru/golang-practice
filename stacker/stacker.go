package main

import "fmt"
import "usecode.ru/stacker/mystack"

func main() {
	var haystack mystack.Stack

	haystack.Push("Hay")
	haystack.Push(-15)
	haystack.Push([]string{"string", "array", "three elements"})
	haystack.Push(48.65)

	//	item, err := haystack.Pop()
	for item, err := haystack.Pop(); err == nil; item, err = haystack.Pop() {
		fmt.Println(item)
		//	item, err := haystack.Pop()
	}

}
