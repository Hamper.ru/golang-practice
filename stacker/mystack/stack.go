/*Пользовательский стек
реализация по книге
Len()
Top()
Push()
Pop()
*/
package mystack

import "errors"

type Stack []interface{}

//длинна стека
func (stack Stack) Len() int {
	return len(stack)
}

//Положить элемент в стек
func (stack *Stack) Push(x interface{}) {
	*stack = append(*stack, x)
}

func (stack Stack) Top() (interface{}, error) {
	if len(stack) == 0 {
		return nil, errors.New("Ошибка Top(). В стеке нет элементов")
	}
	return stack[len(stack)-1], nil
}

func (stack *Stack) Pop() (interface{}, error) {
	theStack := *stack
	if len(theStack) == 0 {
		return nil, errors.New("Ошибка Pop(). В стеке нет элементов")
	}
	x := theStack[len(theStack)-1]
	*stack = theStack[:len(theStack)-1]
	return x, nil
}
