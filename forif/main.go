//Поиск суммы двух чисел из массива равной заданному числу.
package main

import "fmt"

func isSumFind(arr []int, number int) bool {
	for i := 0; i < len(arr)-1; i++ {
		for j := i + 1; j < len(arr); j++ {
			if arr[i]+arr[j] == number {
				return true
			}
		}
	}
	return false
}

func main() {
	qArr := []int{2, 4, 5, 6}
	dArr := []int{9, 3, 5, 6}

	fmt.Println(qArr)
	fmt.Println(dArr)
	fmt.Println(isSumFind(qArr, 11))
	fmt.Println(isSumFind(dArr, 1))

}
