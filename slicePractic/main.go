//создание стека с помощью среза
package main

import "fmt"

type stackRune struct {
	Stack *[]rune
	Size  int
}

func (st *stackRune) New(size int) (err error) {
	if size < 1 {
		err = fmt.Errorf("Размер стека не может быть меньше 1")
	} else {
		*st.Stack = make([]rune, size, size+size/2)
		*st.Size = len(st.Stack)
		fmt.Println("Создан стек емкостью = ", cap(st.Stack))
		err = nil
	}
	return
}

func (st *stackRune) IsEmpty() bool {
	if *st.Size <= 0 {
		return true
	}
	return false
}

func (st *stackRune) Len() int {
	return *st.Size
}

func (st *stackRune) Push(value rune) {
	locSt := &st
	if len(locSt.Stack) == cap(locSt.Stack) {
		locSt.Stack = append(locSt.Stack, value)
		locSt.Size = len(locSt.Stack)
	} else {
		locSt.Stack[locSt.Size] = value
		locSt.Size++
	}
	st = locSt
}

func (st stackRune) Pop() (value rune, err error) {
	if st.IsEmpty() {
		//		value = nil
		err = fmt.Errorf("Стек пустой")
	} else {
		value = st.Stack[st.Size]
		//st.stack[st.stack.Len()] = nil
		err = nil
		st.Size--
	}
	return
}

func main() {
	ru := stackRune{}
	var sRune = &ru
	var d rune
	var stringStart = "Старт программы"
	fmt.Println(stringStart)
	sRune.New(20)
	sRune.Push('А')
	fmt.Println("Размер size = ", sRune.Size)
	fmt.Println("Емкость стека = ", cap(sRune.Stack))
	d = 'Д'

	fmt.Printf("Символ руны d = %c\n", d)
	fmt.Println("Размер заполености стека = ", sRune.Len())
	d, _ = sRune.Pop()
	fmt.Printf("Символ измененной руны = %c\n", d)

	fmt.Println(sRune.Len())
}
